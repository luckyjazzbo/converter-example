#= require jquery
#= require underscore
#= require backbone
#= require d3
#= require_tree .
#= require_self

new ConverterView(
  el: "#converter",
  from: new Scale(
    name: "Celsius"
    min: -50
    max: 200
  ),
  to: new Scale(
    name: "Fahrenheit"
    min: -40
    max: 400
  ),
  converter: (fromValue) ->
    fromValue * 9/5 + 32
).render()
