window.ConverterView = Backbone.View.extend
  template: _.template """
    <label>
      <%= from.get("name") %>
      <input id="value" type="number">
    </label>
    <button id="update">Update</button>

    <h2><%= from.get("name") %></h2>
    <div id="fromScale"></div>

    <h2><%= to.get("name") %></h2>
    <div id="toScale"></div>
  """

  events:
    "change #value": "update"
    "keyup #value": "update"
    "keypress #value": "update"
    "click #update": "update"

  update: ->
    fromValue = parseInt(@$el.find("#value").val(), 10) || 0
    toValue = @options.converter(fromValue)
    @options.from.set "value", fromValue
    @options.to.set "value", toValue
    @updateScales()

  render: ->
    @$el.html @template(@options)
    @updateScales()

  updateScales: ->
    @updateScale("#fromScale", @options.from)
    @updateScale("#toScale", @options.to)

  updateScale: (selector, scale) ->
    margin =
      top: 30
      right: 30
      bottom: 10
      left: 10

    width = 1024 - margin.left - margin.right
    height = 50 - margin.top - margin.bottom

    if @$(selector + " svg").length > 0
      svg = d3.select(selector + " svg g")
      $(selector + " .x.axis").remove()
    else
      svg = d3.select(selector).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

    x = d3.scale.linear()
      .domain([
        Math.min(scale.get("min"), scale.get("value") * 1.2),
        Math.max(scale.get("max"), scale.get("value") * 1.2)])
      .range([0, width])

    bar = svg.selectAll(".bar")
      .data([scale.get("value")])

    bar.enter()
      .append("rect")
        .attr("class", "bar")
        .attr("fill", (value) -> if value < 0 then "brown" else "steelblue")
        .attr("x", (value) -> x(Math.min(0, value)))
        .attr("width", (value) -> Math.abs(x(value) - x(0)))
      .attr("height", 20)

    bar.transition()
      .duration(500)
        .attr("fill", (value) -> if value < 0 then "brown" else "steelblue")
        .attr("x", (value) -> x(Math.min(0, value)))
        .attr("width", (value) -> Math.abs(x(value) - x(0)))

    xAxis = d3.svg.axis()
      .scale(x)
      .orient("top")
    svg.append("g")
      .attr("class", "x axis")
      .call(xAxis)
